# Treelogism

## Introduction :

Treelogism est une application qui a pour but de réduire le nombre de déchets sur la planète.

## Fonctionnement :

Treelogism est développé sous React avec le gestionnaire d'état Redux.
Le projet comprends un dossier `src` où se situent tout les composents, les images et les fichiers de styles.
Dans le dossier composant, il y a un dossier actions qui comprend tout les types d'actions qui seront appellées par Redux.
Ensuite chaque composent a son propre dossier, composer de `index.js` : la class, puis deux dossiers :
* `components` : comprend tout les sous éléments du composant parent.
* `actions` : comprend toutes les actions qui feront des requêtes API.

## Pré-requis :

Pour visualiser ce projet, vous avez besoin de :
* `npm`.

Pour développer dessus, vous avez besoin de :
* l'extension React-Redux sur votre navigateur.
* Des connaissances en React puis en Redux.
* Des connaissances pour le respect des normes de codage de Airbnb

## Utilisation :

* `git clone https://gitlab.com/suleydogan70/treelogism.git`
* `cd treelogism`
* `npm install`
* `npm start` or change the scripts in the package.json

## Actions disponibles :

* Se connecter
* S'inscrire
* Ajouter un post
* Liker un post
* Supprimer un post
* Pin la position actuelle
* Ajouter un ami
* Supprimer un ami
* Visualiser une carte où les autres ont pin
* Voir une notification
* Supprimer une notification
* Voir le profil d'un utilisateur
* Modifier son profil
