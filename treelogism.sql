-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 05 Décembre 2018 à 21:56
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `treelogism`
--

-- --------------------------------------------------------

--
-- Structure de la table `friends`
--

CREATE TABLE `friends` (
  `user_id` bigint(20) NOT NULL,
  `friend_id` bigint(20) NOT NULL,
  `friendship_state` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `friends`
--

INSERT INTO `friends` (`user_id`, `friend_id`, `friendship_state`) VALUES
(37, 39, 1),
(37, 40, 0);

-- --------------------------------------------------------

--
-- Structure de la table `liked_posts`
--

CREATE TABLE `liked_posts` (
  `user_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `liked_posts`
--

INSERT INTO `liked_posts` (`user_id`, `post_id`) VALUES
(36, 1),
(37, 1),
(38, 1),
(36, 2),
(37, 2),
(36, 3),
(37, 3);

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

CREATE TABLE `notifications` (
  `user_id` bigint(20) NOT NULL,
  `notification_type_id` bigint(20) NOT NULL,
  `notification_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `notification_types`
--

CREATE TABLE `notification_types` (
  `notification_type_id` bigint(20) NOT NULL,
  `notification_type_text` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `position`
--

CREATE TABLE `position` (
  `position_id` bigint(20) NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `post_id` bigint(20) NOT NULL,
  `post_publication_datetime` datetime DEFAULT NULL,
  `post_image` varchar(150) DEFAULT 'publication.jpg',
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `posts`
--

INSERT INTO `posts` (`post_id`, `post_publication_datetime`, `post_image`, `user_id`) VALUES
(1, '2018-12-01 16:38:53', 'publication.jpg', 37),
(2, '2018-12-01 16:39:22', 'publication.jpg', 37),
(3, '2018-11-23 00:00:00', 'publication.jpg', 36),
(4, '2018-10-26 00:00:00', 'publication.jpg', 38),
(5, '2018-09-25 00:00:00', 'publication.jpg', 38),
(6, '2018-12-02 00:00:00', 'publication.jpg', 38),
(7, '2018-12-02 23:05:46', 'publication.jpg', 37),
(8, '2016-11-17 07:18:06', 'publication.jpg', 38);

-- --------------------------------------------------------

--
-- Structure de la table `titles`
--

CREATE TABLE `titles` (
  `title_id` bigint(20) NOT NULL,
  `title_name` varchar(15) DEFAULT NULL,
  `title_max_leafs` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `titles`
--

INSERT INTO `titles` (`title_id`, `title_name`, `title_max_leafs`) VALUES
(1, 'Amateur', 250),
(2, 'Protecteur', 700),
(3, 'Saint', 1350);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL,
  `user_mail` varchar(150) DEFAULT NULL,
  `user_psw` varchar(150) DEFAULT NULL,
  `user_firstname` varchar(30) DEFAULT NULL,
  `user_lastname` varchar(30) DEFAULT NULL,
  `user_avatar` varchar(150) NOT NULL DEFAULT 'profil.jpg',
  `user_description` varchar(300) DEFAULT NULL,
  `admin` int(1) DEFAULT '0',
  `user_leafs` int(5) DEFAULT '0',
  `title_id` bigint(20) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`user_id`, `user_mail`, `user_psw`, `user_firstname`, `user_lastname`, `user_avatar`, `user_description`, `admin`, `user_leafs`, `title_id`) VALUES
(36, 'suleyman.dogan@gmail.com', '$2b$10$LUttoyIHZK4qsRU4Dc6CW.qX/iSohpYuf13cpq5r8C/A/bA3EhOyG', 'Suleyman', 'Dogan', 'profil.jpg', NULL, 0, 0, 1),
(37, 'nudoru70@gmail.com', '$2b$10$viTiT0Yc982lFIIqFvWmU.T0xU3CeCD5xYq4RvJsgbmh5lto8t1DG', 'Suleyman', 'Dogan', 'profil.jpg', NULL, 0, 0, 1),
(38, 'papa@gmail.com', '$2b$10$TZt/SExtScjhXjUFsYKMo.RCVUuuMvXjC.c8e7htKfj/m6HnSxjiO', 'Suleyman', 'Dogan', 'profil.jpg', NULL, 0, 0, 1),
(39, 'louisbarberot@gmail.com', 'low1234!', 'Low', 'Barberot', 'profil.jpg', 'Hello, i\'m new on this social network. I always wanted to save the earth. Thanks to Treelogism, my dreams will become true !', 0, 0, 1),
(40, 'carolinechambre@gmail.com', 'caro123!', 'Caroline', 'Chambre', 'profil.jpg', 'Heyy ! I\'m Caroline, and my favorite word is \'meuh\' hahahaha !', 0, 0, 1),
(41, 'alexcolombel@gmail.com', 'ouvrerlaporte21', 'Alexis', 'Colombel', 'profil.jpg', 'I want to be anonymous ', 0, 0, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`user_id`,`friend_id`),
  ADD KEY `friend_id` (`friend_id`);

--
-- Index pour la table `liked_posts`
--
ALTER TABLE `liked_posts`
  ADD PRIMARY KEY (`user_id`,`post_id`),
  ADD KEY `FK_liked_posts_post_id` (`post_id`);

--
-- Index pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`user_id`,`notification_type_id`),
  ADD KEY `FK_notifications_notification_type_id` (`notification_type_id`);

--
-- Index pour la table `notification_types`
--
ALTER TABLE `notification_types`
  ADD PRIMARY KEY (`notification_type_id`);

--
-- Index pour la table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`position_id`),
  ADD KEY `FK_position_user_id` (`user_id`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `FK_posts_user_id` (`user_id`);

--
-- Index pour la table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`title_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `FK_users_title_id` (`title_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `friends`
--
ALTER TABLE `friends`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pour la table `liked_posts`
--
ALTER TABLE `liked_posts`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT pour la table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `notification_types`
--
ALTER TABLE `notification_types`
  MODIFY `notification_type_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `position`
--
ALTER TABLE `position`
  MODIFY `position_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `titles`
--
ALTER TABLE `titles`
  MODIFY `title_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `liked_posts`
--
ALTER TABLE `liked_posts`
  ADD CONSTRAINT `FK_liked_posts_post_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`),
  ADD CONSTRAINT `FK_liked_posts_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Contraintes pour la table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `FK_notifications_notification_type_id` FOREIGN KEY (`notification_type_id`) REFERENCES `notification_types` (`notification_type_id`),
  ADD CONSTRAINT `FK_notifications_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Contraintes pour la table `position`
--
ALTER TABLE `position`
  ADD CONSTRAINT `FK_position_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Contraintes pour la table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `FK_posts_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_users_title_id` FOREIGN KEY (`title_id`) REFERENCES `titles` (`title_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
