import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as actions from '../actions'

class Friend extends Component {
  constructor(props) {
    super(props)
    this.state = {
      authRoute: true
    }
  }

  componentWillMount() {
    if (window.location.pathname !== '/amis') {
      this.state.authRoute = false
    }
  }

  validateFriendship(friendId) {
    const { validateFriendship, user } = this.props
    validateFriendship(friendId, user.user_firstname, user.user_lastname)
  }

  delete(friendId) {
    const { deleteFriend } = this.props
    if (window.confirm('Delete this friend ?')) {
      deleteFriend(friendId)
    }
  }

  render() {
    const { authRoute } = this.state
    const { item, user, match } = this.props
    if (user && (user.user_id !== parseInt(match, 10)) && (!authRoute && !item.friendship_state)) {
      return ''
    }
    return (
      <div className="bloc bloc_ami">
        <div className="infos">
          <div className="profil">
            <Link to={`/profil/${item.user_id}`}><img src={`http://localhost:1234/uploads/${item.user_avatar}`} alt={`${item.user_firstname} ${item.user_lastname}`} /></Link>
          </div>
          <div className="texte">
            <p className="name"><Link to={`/profil/${item.user_id}`}>{`${item.user_firstname} ${item.user_lastname}`}</Link></p>
            <p className="rang">{item.title_name}</p>
            <div className="experience">
              <p className="nb_points">
                <span className="requis">{item.user_leafs}</span>
              </p>
            </div>
          </div>
          <Link to={`/profil/${item.user_id}`} type="button" title="Consulter le profil" className="btn btn_picto look" />
          {
            !item.friendship_state
            && !item.sender
            && (authRoute
            || (user && user.user_id === parseInt(match, 10) && !authRoute))
              ? <button type="button" title="Accepter cette personne" className="btn btn_picto check" onClick={() => this.validateFriendship(item.user_id)} />
              : ''
          }
          {
            authRoute
            || (user && user.user_id === parseInt(match, 10) && !authRoute)
              ? <button type="button" title="Retirer de vos amis" className="btn btn_picto delete" onClick={() => this.delete(item.user_id)} />
              : ''
          }
        </div>
        {
          !item.friendship_state
          && item.sender
            ? <p className="attente">Demande d&#39;amis envoyée. Réponse en attente.</p>
            : ''
        }
        {
          !item.friendship_state
          && !item.sender
            ? <p className="attente">Vous avez une demande d&#39;amis en attente.</p>
            : ''
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.auth.user
  }
}

export default connect(mapStateToProps, actions)(Friend)
