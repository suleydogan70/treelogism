import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as actions from '../actions'

class NewFriend extends Component {
  add(friendId) {
    const { addFriend, auth: { user } } = this.props
    addFriend(friendId, user.user_firstname, user.user_lastname)
  }

  render() {
    const { item } = this.props
    return (
      <div className="bloc bloc_ami">
        <div className="infos">
          <div className="profil">
            <Link to={`/profil/${item.user_id}`}><img src={`http://localhost:1234/uploads/${item.user_avatar}`} alt={`${item.user_firstname} ${item.user_lastname}`} /></Link>
          </div>
          <div className="texte">
            <p className="name"><Link to={`/profil/${item.user_id}`}>{`${item.user_firstname} ${item.user_lastname}`}</Link></p>
            <p className="rang">{item.title_name}</p>
            <div className="experience">
              <p className="nb_points">
                <span className="requis">{item.user_leafs}</span>
              </p>
            </div>
          </div>
          <Link to={`profil/${item.user_id}`} type="button" title="Consulter le profil" className="btn btn_picto look" />
          <button type="button" title="Ajouter un ami" className="btn btn_picto add" onClick={() => this.add(item.user_id)} />
        </div>
      </div>
    )
  }
}

export default connect(state => state, actions)(NewFriend)
