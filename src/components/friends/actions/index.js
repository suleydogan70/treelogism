import axios from 'axios'
import actionsType from '../../actions/types'

const dispatcher = (dispatch, response) => {
  dispatch({
    type: actionsType.GET_FRIENDS,
    data: response.data.friends
  })
  dispatch({
    type: actionsType.GET_PROFILE_FRIENDS,
    data: response.data.friends
  })
  dispatch({
    type: actionsType.GET_SUGGESTIONS,
    data: response.data.suggestions
  })
}

export const getUserFriends = () => (
  async (dispatch) => {
    try {
      const token = localStorage.getItem('token')
      const headers = {
        Authorization: `Bearer ${token}`
      }
      const response = await axios.post('http://localhost:1234/friends/', null, { headers })
      if (response.data.err) {
        return dispatch({
          type: actionsType.ERROR_GET_FRIENDS,
          data: response.data.err
        })
      }
      return dispatcher(dispatch, response)
    } catch (error) {
      dispatch({
        type: actionsType.ERROR_GET_FRIENDS,
        data: 'Impossible to get your friend list'
      })
    }
    return true
  }
)

export const addFriend = (friendId, firstname, lastname) => (
  async (dispatch) => {
    try {
      const token = localStorage.getItem('token')
      const headers = {
        Authorization: `Bearer ${token}`
      }
      const user = `${firstname} ${lastname}`
      const response = await axios.post('http://localhost:1234/friends/add_friend', { friendId, user }, { headers })
      if (response.data.err) {
        return dispatch({
          type: actionsType.ERROR_GET_FRIENDS,
          data: response.data.err
        })
      }
      return dispatcher(dispatch, response)
    } catch (error) {
      dispatch({
        type: actionsType.ERROR_GET_FRIENDS,
        data: 'Impossible to get your friend list'
      })
    }
    return true
  }
)

export const validateFriendship = (friendId, firstname, lastname) => (
  async (dispatch) => {
    try {
      const token = localStorage.getItem('token')
      const headers = {
        Authorization: `Bearer ${token}`
      }
      const user = `${firstname} ${lastname}`
      const response = await axios.post('http://localhost:1234/friends/validate_friend', { friendId, user }, { headers })
      if (response.data.err) {
        return dispatch({
          type: actionsType.ERROR_GET_FRIENDS,
          data: response.data.err
        })
      }
      return dispatcher(dispatch, response)
    } catch (error) {
      dispatch({
        type: actionsType.ERROR_GET_FRIENDS,
        data: 'Impossible to get your friend list'
      })
    }
    return true
  }
)

export const deleteFriend = friendId => (
  async (dispatch) => {
    try {
      const token = localStorage.getItem('token')
      const headers = {
        Authorization: `Bearer ${token}`
      }
      const response = await axios.post('http://localhost:1234/friends/delete_friend', { friendId }, { headers })
      if (response.data.err) {
        return dispatch({
          type: actionsType.ERROR_GET_FRIENDS,
          data: response.data.err
        })
      }
      return dispatcher(dispatch, response)
    } catch (error) {
      dispatch({
        type: actionsType.ERROR_GET_FRIENDS,
        data: 'Impossible to get your friend list'
      })
    }
    return true
  }
)
