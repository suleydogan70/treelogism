import React, { Component } from 'react'
import { connect } from 'react-redux'
import Friend from './components/friend'
import NewFriend from './components/new_friend'
import * as actions from './actions'

class Friends extends Component {
  componentWillMount() {
    const { getUserFriends } = this.props
    getUserFriends()
  }

  render() {
    const { friends: { friends, suggestions } } = this.props
    let friendRender
    let suggestionRender
    if (friends.length !== 0) {
      friendRender = friends.map(item => <Friend key={item.user_id} item={item} />)
    } else {
      friendRender = <p className="error">Vous n&apos;avez pas d&apos;amis... Prenez donc un curly ! :)</p>
    }
    if (suggestions.length !== 0) {
      suggestionRender = suggestions.map(item => <NewFriend key={item.user_id} item={item} />)
    } else {
      suggestionRender = <p className="error">Aucune suggestion disponible.</p>
    }
    return (
      <div id="online">
        <div className="wrap">
          <div className="row online_template" id="template_amis">
            <div>
              <h2 className="title_picto picto_friends">
                <span>{friends.length}</span>
                <span> amis</span>
              </h2>
              {friendRender}
            </div>
            <div>
              <h2 className="title_picto picto_friends_add">Suggestions</h2>
              <div className="bloc border bloc_profil">
                {suggestionRender}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    friends: state.friends
  }
}

export default connect(mapStateToProps, actions)(Friends)
