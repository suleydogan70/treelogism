import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import * as actions from './actions'

class Header extends Component {
  constructor(props) {
    super(props)
    this.getNotifications = this.getNotifications.bind(this)
    this.notifs = setInterval(this.getNotifications, 30000)
    this.state = {
      searchedUsers: []
    }
  }

  componentWillMount() {
    const {
      auth: { user },
      getUserWithToken,
      getAllUsers
    } = this.props
    if (!user) {
      getUserWithToken()
    }
    this.getNotifications()
    getAllUsers()
      .then(response => this.setState({ searchedUsers: response }))
  }

  getNotifications() {
    const { getUserNotifications } = this.props
    getUserNotifications()
  }

  disconnectUser() {
    const { disconnectUser } = this.props
    clearInterval(this.notifs)
    if (disconnectUser()) {
      return <Redirect to="/connexion" />
    }
    return false
  }

  dismissNotif(notifId) {
    const { dismissNotification } = this.props
    dismissNotification(notifId)
  }

  focusInput() {
    document.getElementById('showUserList').classList.add('active')
  }

  focusOutInput(name) {
    document.getElementById('showUserList').classList.remove('active')
    if (name) {
      document.getElementById('search_bar').value = name
    }
  }

  handleOnChange(e) {
    const { header: { users } } = this.props
    let searchedUsers = users
    if (e.target.value.length > 0) {
      const regex = new RegExp(`^${e.target.value}`, 'i')
      searchedUsers = users.sort().filter(item => regex.test(item.fullname))
    }
    this.setState({ searchedUsers })
  }

  render() {
    const { auth: { isAuth, user }, notifications } = this.props
    const { searchedUsers } = this.state
    let profilLink
    if (!isAuth) {
      const token = localStorage.getItem('token')
      if (!token || token === null) {
        return <Redirect to="/connexion" />
      }
    }
    if (user) {
      profilLink = (
        <div>
          <Link to={`/profil/${user.user_id}`} className="img">
            <img src={`http://localhost:1234/uploads/${user.user_avatar}`} alt={`${user.user_firstname} ${user.user_lastname}`} />
          </Link>
          <Link to={`/profil/${user.user_id}`} className="user_name">
            <span className="user_name">{`${user.user_firstname} ${user.user_lastname}`}</span>
          </Link>
          <p className="deconnexion" onClick={() => this.disconnectUser()}>
            <span className="picto_deconnexion" />
            <span>Déconnexion</span>
          </p>
        </div>
      )
    } else {
      profilLink = <Link to="/profil/" className="img"><img src="http://localhost:1234/uploads/profil.jpg" alt="" /></Link>
    }
    return (
      <header>
        <div className="wrap">
          <div className="header_left">
            <div id="logo">
              <Link to="/" className="img"><img src="../src/img/logo.png" alt="Treelogism" /></Link>
            </div>
            <div id="search">
              <form onSubmit={e => e.preventDefault()}>
                <label onFocus={() => this.focusInput()} onBlur={() => this.focusOutInput()} htmlFor="search_bar">
                  <input onChange={e => this.handleOnChange(e)} id="search_bar" type="text" placeholder="Rechercher..." />
                  <ul id="showUserList" className="label show-for-sr">
                    {
                      searchedUsers.map(aUser => <li onClick={() => this.focusOutInput(aUser.fullname)} key={aUser.user_id}><Link to={`/profil/${aUser.user_id}`}>{aUser.fullname}</Link></li>)
                    }
                  </ul>
                </label>
                <input type="submit" value="Rechercher" />
              </form>
            </div>
          </div>
          <div className="header_right">
            <nav>
              <ul>
                <li>
                  <Link className="picto_publication" to="/publication">
                    <span className="intitule">nouveau</span>
                  </Link>
                </li>
                <li>
                  <Link className="picto_amis" to="/amis">
                    <span className="intitule">amis</span>
                  </Link>
                </li>
                <li>
                  <Link className="picto_map" to="/map">
                    <span className="intitule">carte</span>
                  </Link>
                </li>
                <li className="notifications_container">
                  <p className="picto_notification" to="/">
                    {
                      notifications.data.length
                        ? <span className="notification">{ notifications.data.length }</span>
                        : ''
                    }
                    <span className="intitule">notifs</span>
                  </p>
                  <div className="notifications">
                    {
                      !notifications.data.length
                        ? <span><a><span>Aucune notifications disponible.</span></a></span>
                        : ''
                    }
                    {
                      notifications.data.map(item => (
                        <span key={item.id}>
                          {
                            (() => {
                              switch (item.type) {
                                case 1:
                                case 2:
                                  return (
                                    <Link className="notif_link" to="/amis">
                                      <span>
                                        {item.text}
                                        <span onClick={() => this.dismissNotif(item.id)} className="dismiss_notif">x</span>
                                      </span>
                                    </Link>
                                  )
                                default:
                                  return (
                                    <Link className="notif_link" to="/">
                                      <span>
                                        {item.text}
                                        <span onClick={() => this.dismissNotif(item.id)} className="dismiss_notif">x</span>
                                      </span>
                                    </Link>
                                  )
                              }
                            })()
                          }
                          <hr />
                        </span>
                      ))
                    }
                  </div>
                </li>
              </ul>
            </nav>
            <div id="profil">
              {profilLink}
            </div>
          </div>
        </div>
      </header>
    )
  }
}

export default connect(state => state, actions)(Header)
