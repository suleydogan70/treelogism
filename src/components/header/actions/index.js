import axios from 'axios'
import actionsType from '../../actions/types'

export const getUserWithToken = () => (
  (dispatch) => {
    const token = localStorage.getItem('token')
    if (token === null) {
      return dispatch({
        type: actionsType.CONNEXION_FAIL,
        data: 'The user is not logged in'
      })
    }
    const headers = {
      Authorization: `Bearer ${token}`
    }
    axios.post('http://localhost:1234/users/getUser', null, {
      headers
    })
      .then(
        (response) => {
          if (response.data.err) {
            return dispatch({
              type: actionsType.CONNEXION_FAIL,
              data: response.data.err
            })
          }
          return dispatch({
            type: actionsType.CONNECT_USER,
            data: response.data
          })
        },
        () => (
          dispatch({
            type: actionsType.CONNEXION_FAIL,
            data: 'An error has occured'
          })
        )
      )
    return true
  }
)

export const getAllUsers = () => (
  async (dispatch) => {
    try {
      const response = await axios.get('http://localhost:1234/users/allUsers')
      dispatch({
        type: actionsType.GET_ALL_USERS,
        data: response.data.users
      })
      return response.data.users
    } catch (error) {
      dispatch({
        type: actionsType.ERROR_GET_ALL_USERS,
        data: 'Cannot get all users'
      })
    }
    return ''
  }
)

export const getUserNotifications = () => (
  async (dispatch) => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    try {
      const response = await axios.post('http://localhost:1234/notifications/', null, { headers })
      if (response.data.err) {
        return dispatch({
          type: actionsType.ERROR_GET_NOTIFICATIONS,
          data: response.data.err
        })
      }
      dispatch({
        type: actionsType.GET_NOTIFICATIONS,
        data: response.data.notifications
      })
    } catch (error) {
      dispatch({
        type: actionsType.ERROR_GET_NOTIFICATIONS,
        data: error
      })
    }
    return true
  }
)

export const dismissNotification = notifId => (
  async (dispatch) => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    try {
      const response = await axios.post('http://localhost:1234/notifications/dismiss_notif', { notifId }, { headers })
      if (response.data.err) {
        return dispatch({
          type: actionsType.ERROR_GET_NOTIFICATIONS,
          data: response.data.err
        })
      }
      dispatch({
        type: actionsType.GET_NOTIFICATIONS,
        data: response.data.notifications
      })
    } catch (error) {
      dispatch({
        type: actionsType.ERROR_GET_NOTIFICATIONS,
        data: error
      })
    }
    return true
  }
)

export const disconnectUser = () => (
  (dispatch) => {
    localStorage.removeItem('token')
    dispatch({
      type: actionsType.DELETE_ALL,
      data: ''
    })
    return true
  }
)
