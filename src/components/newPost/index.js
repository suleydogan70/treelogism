import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import WebcamCapture from './components/webcam'

class NewPost extends Component {
  constructor(props) {
    super(props)
    this.state = {
      webcam: null,
      imgUrl: null,
      pin: false,
      latitude: null,
      longitude: null
    }
    this.closeModal = this.closeModal.bind(this)
    this.getCurrentPosition = this.getCurrentPosition.bind(this)
  }

  componentWillMount() {
    navigator.getUserMedia = navigator.getUserMedia
    || navigator.webkitGetUserMedia
    || navigator.mozGetUserMedia
    || navigator.msGetUserMedia
    || navigator.oGetUserMedia
    if (navigator.getUserMedia) {
      this.setState({ webcam: <WebcamCapture /> })
    } else {
      this.setState({ webcam: <p>Webcam disabled.</p> })
    }
  }

  getCurrentPosition(position) {
    const { latitude, longitude } = position.coords
    this.setState({ latitude, longitude })
  }

  togglePin() {
    const { pin } = this.state
    this.setState({ pin: !pin })
    if (!pin && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.getCurrentPosition)
    }
  }

  closeModal() {
    document.getElementById('postImg').value = null
    this.setState({ imgUrl: null })
  }

  handleSubmit(e) {
    e.preventDefault()
    const formData = new FormData()
    const {
      newPost,
      history,
      auth: { user },
      pinPosition
    } = this.props
    const {
      latitude,
      longitude,
      pin
    } = this.state
    formData.append('file', this.fileInput.files[0])
    if (newPost(formData, user.user_firstname, user.user_lastname, true)) {
      if (pin) {
        pinPosition(latitude, longitude)
      }
      history.push('/')
    }
  }

  showFile() {
    this.setState({ imgUrl: URL.createObjectURL(this.fileInput.files[0]) })
  }

  render() {
    const { webcam, imgUrl } = this.state
    let showImage
    if (imgUrl) {
      showImage = (
        <div>
          <span className="closeModal" onClick={() => this.closeModal()}><span>X</span></span>
          <img src={imgUrl} alt="ready to be uploaded" />
          <div>
            <span>Poster une photo vous rapporte 75 feuilles !</span>
            <br />
            <span>Pinner ma position pour montrer l&#39;endroit nettoyé</span>
            <label htmlFor="pinCheckbox" className="switch">
              <input onChange={() => this.togglePin()} id="pinCheckbox" type="checkbox" />
              <span className="slider round" />
            </label>
          </div>
          <button className="btn saveButton" type="submit"><span>Poster cette photo</span></button>
        </div>
      )
    }
    return (
      <div id="online">
        <div className="wrap">
          <div className="newPost">
            { webcam }
            <hr />
            <form onSubmit={e => this.handleSubmit(e)} encType="multipart/form-data">
              <label className="btn saveButton" htmlFor="postImg">
                <span>Parcourir vos fichiers !</span>
                <input hidden id="postImg" ref={(ref) => { this.fileInput = ref }} onChange={() => this.showFile()} type="file" />
              </label>
              <div className={`thumbnail ${(showImage ? 'active' : '')}`}>
                { showImage }
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(state => state, actions)(NewPost)
