import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import Webcam from 'react-webcam'
import * as actions from '../actions'

class WebcamCapture extends Component {
  constructor(props) {
    super(props)
    this.state = {
      imgSrc: null,
      redirect: false,
      pin: false,
      latitude: null,
      longitude: null
    }
    this.setRef = this.setRef.bind(this)
    this.capture = this.capture.bind(this)
    this.getCurrentPosition = this.getCurrentPosition.bind(this)
  }

  getCurrentPosition(position) {
    const { latitude, longitude } = position.coords
    this.setState({ latitude, longitude })
  }

  setRef(webcam) {
    this.webcam = webcam
  }

  togglePin() {
    const { pin } = this.state
    this.setState({ pin: !pin })
    if (!pin && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.getCurrentPosition)
    }
  }

  capture() {
    const imageSrc = this.webcam.getScreenshot()
    this.setState({ imgSrc: imageSrc })
  }

  send(e) {
    e.preventDefault()
    const {
      imgSrc,
      pin,
      latitude,
      longitude
    } = this.state
    const {
      newPost,
      auth: { user },
      pinPosition
    } = this.props
    newPost(imgSrc.split(',')[1], user.user_firstname, user.user_lastname)
    if (pin) {
      pinPosition(latitude, longitude)
    }
    this.setState({ redirect: true })
  }

  closeModal() {
    this.setState({ imgSrc: null })
  }

  render() {
    const { imgSrc, redirect } = this.state
    const videoConstraints = {
      width: 315,
      height: 315,
      facingMode: 'user'
    }
    let img
    if (imgSrc !== null) {
      img = (
        <div>
          <span className="closeModal" onClick={() => this.closeModal()}><span>X</span></span>
          <img src={imgSrc} alt="Screenshot" />
          <div>
            <span>Poster une photo vous rapporte 75 feuilles !</span>
            <br />
            <span>Pinner ma position pour montrer l&#39;endroit nettoyé</span>
            <label htmlFor="pinCheckbox" className="switch">
              <input onChange={() => this.togglePin()} id="pinCheckbox" type="checkbox" />
              <span className="slider round" />
            </label>
          </div>
          <button className="btn saveButton" type="button" onClick={e => this.send(e)}><span>Poster cette photo</span></button>
        </div>
      )
    }

    if (redirect) {
      return <Redirect to="/" />
    }

    return (
      <div>
        <Webcam
          audio={false}
          height={350}
          ref={this.setRef}
          screenshotFormat="image/jpeg"
          width={350}
          videoConstraints={videoConstraints}
        />
        <button className="btn saveButton" type="button" onClick={this.capture}><span>Prendre la photo !</span></button>
        <div className={`thumbnail ${(imgSrc ? 'active' : '')}`}>
          {img}
        </div>
      </div>
    )
  }
}

export default connect(state => state, actions)(WebcamCapture)
