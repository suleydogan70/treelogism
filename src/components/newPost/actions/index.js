import axios from 'axios'
import actionsType from '../../actions/types'

const formatLikedPosts = likedPostsArray => (
  likedPostsArray.map(post => (
    post.post_id
  ))
)

export const newPost = (img, firstname, lastname, file = false) => (
  (dispatch) => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    const user = `${firstname} ${lastname}`
    if (file) {
      axios.post('http://localhost:1234/posts/newPostFile', img, { headers })
        .then(
          (response) => {
            if (response.data.err) {
              return dispatch({
                type: actionsType.ERROR_GET_POSTS,
                data: response.data.err
              })
            }
            dispatch({
              type: actionsType.GET_POSTS,
              data: response.data.posts
            })
            return dispatch({
              type: actionsType.GET_LIKED_POSTS,
              data: formatLikedPosts(response.data.liked_posts)
            })
          },
          () => {
            dispatch({
              type: actionsType.ERROR_GET_POSTS,
              data: 'Impossible to fetch posts'
            })
            return dispatch({
              type: actionsType.ERROR_GET_LIKED_POSTS,
              data: 'Impossible to get liked posts'
            })
          }
        )
    } else {
      axios.post('http://localhost:1234/posts/newPostTxt', { img, user }, { headers })
        .then(
          (response) => {
            if (response.data.err) {
              return dispatch({
                type: actionsType.ERROR_GET_POSTS,
                data: response.data.err
              })
            }
            dispatch({
              type: actionsType.GET_POSTS,
              data: response.data.posts
            })
            return dispatch({
              type: actionsType.GET_LIKED_POSTS,
              data: formatLikedPosts(response.data.liked_posts)
            })
          },
          () => {
            dispatch({
              type: actionsType.ERROR_GET_POSTS,
              data: 'Impossible to fetch posts'
            })
            return dispatch({
              type: actionsType.ERROR_GET_LIKED_POSTS,
              data: 'Impossible to get liked posts'
            })
          }
        )
    }
    return true
  }
)

export const pinPosition = (latitude, longitude) => (
  () => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    axios.post('http://localhost:1234/maps/pin', { latitude, longitude }, { headers })
  }
)
