import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from './actions'
import Publication from './components/posts'

class Home extends Component {
  componentDidMount() {
    const { getPosts } = this.props
    getPosts()
  }

  render() {
    const { posts } = this.props
    return (
      <div id="online">
        <div className="wrap">
          {
            !posts.length
              ? <p className="error">Aucune publication disponible. Publiez votre premier post !</p>
              : ''
          }
          <div className="row online_template" id="template_home">
            {posts.map(post => <Publication key={post.post_id} item={post} />)}
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    posts: state.posts.posts
  }
}

export default connect(mapStateToProps, actions)(Home)
