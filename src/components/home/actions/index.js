import axios from 'axios'
import actionsType from '../../actions/types'

const formatLikedPosts = likedPostsArray => (
  likedPostsArray.map(post => (
    post.post_id
  ))
)

export const getPosts = () => (
  (dispatch) => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    axios.post('http://localhost:1234/posts/', null, { headers })
      .then(
        (response) => {
          if (response.data.err) {
            return dispatch({
              type: actionsType.ERROR_GET_POSTS,
              data: response.data.err
            })
          }
          dispatch({
            type: actionsType.GET_POSTS,
            data: response.data.posts
          })
          return dispatch({
            type: actionsType.GET_LIKED_POSTS,
            data: formatLikedPosts(response.data.liked_posts)
          })
        },
        () => {
          dispatch({
            type: actionsType.ERROR_GET_POSTS,
            data: 'Impossible to fetch posts'
          })
          return dispatch({
            type: actionsType.ERROR_GET_LIKED_POSTS,
            data: 'Impossible to get liked posts'
          })
        }
      )
    return true
  }
)

export const userLikePost = (postId, firstname, lastname) => (
  (dispatch) => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    const user = `${firstname} ${lastname}`
    axios.post('http://localhost:1234/posts/like_post', { postId, user }, { headers })
      .then(
        (response) => {
          if (response.data.err) {
            return dispatch({
              type: actionsType.ERROR_GET_POSTS,
              data: response.data.err
            })
          }
          dispatch({
            type: actionsType.GET_POSTS,
            data: response.data.posts
          })
          return dispatch({
            type: actionsType.GET_LIKED_POSTS,
            data: formatLikedPosts(response.data.liked_posts)
          })
        },
        () => {
          dispatch({
            type: actionsType.ERROR_GET_POSTS,
            data: 'Impossible to fetch posts'
          })
          return dispatch({
            type: actionsType.ERROR_GET_LIKED_POSTS,
            data: 'Impossible to get liked posts'
          })
        }
      )
    return true
  }
)

export const userDislikePost = postId => (
  (dispatch) => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    axios.post('http://localhost:1234/posts/dislike_post', { postId }, { headers })
      .then(
        (response) => {
          if (response.data.err) {
            return dispatch({
              type: actionsType.ERROR_GET_POSTS,
              data: response.data.err
            })
          }
          dispatch({
            type: actionsType.GET_POSTS,
            data: response.data.posts
          })
          return dispatch({
            type: actionsType.GET_LIKED_POSTS,
            data: formatLikedPosts(response.data.liked_posts)
          })
        },
        () => {
          dispatch({
            type: actionsType.ERROR_GET_POSTS,
            data: 'Impossible to fetch posts'
          })
          return dispatch({
            type: actionsType.ERROR_GET_LIKED_POSTS,
            data: 'Impossible to get liked posts'
          })
        }
      )
    return true
  }
)

export const userDeletePost = postId => (
  (dispatch) => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    axios.post('http://localhost:1234/posts/delete_post', { postId }, { headers })
      .then(
        (response) => {
          if (response.data.err) {
            return dispatch({
              type: actionsType.ERROR_GET_POSTS,
              data: response.data.err
            })
          }
          dispatch({
            type: actionsType.GET_POSTS,
            data: response.data.posts
          })
          return dispatch({
            type: actionsType.GET_LIKED_POSTS,
            data: formatLikedPosts(response.data.liked_posts)
          })
        },
        () => {
          dispatch({
            type: actionsType.ERROR_GET_POSTS,
            data: 'Impossible to fetch posts'
          })
          return dispatch({
            type: actionsType.ERROR_GET_LIKED_POSTS,
            data: 'Impossible to get liked posts'
          })
        }
      )
    return true
  }
)
