import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as actions from '../actions'

class Posts extends Component {
  likePost(postId) {
    const { userLikePost, user } = this.props
    userLikePost(postId, user.user_firstname, user.user_lastname)
  }

  dislikePost(postId) {
    const { userDislikePost } = this.props
    userDislikePost(postId)
  }

  deletePost(postId) {
    const { userDeletePost } = this.props
    if (window.confirm('Delete this post ?')) {
      userDeletePost(postId)
    }
  }

  render() {
    const { item, likedPosts, user } = this.props
    let dateRender
    let likePostButton
    let deletePostButton
    if (item.post_date < 60) {
      dateRender = <p className="time">{`Il y a ${Math.floor(item.post_date)} secondes`}</p>
    }
    if (item.post_date < 3600 && item.post_date > 60) {
      dateRender = <p className="time">{`Il y a ${Math.floor(item.post_date / 60)} minutes`}</p>
    }
    if (item.post_date < 86400 && item.post_date > 3600) {
      dateRender = <p className="time">{`Il y a ${Math.floor(item.post_date / 3600)} heure(s)`}</p>
    }
    if (item.post_date < 2678400 && item.post_date > 86400) {
      dateRender = <p className="time">{`Il y a ${Math.floor(item.post_date / 86400)} jour(s)`}</p>
    }
    if (item.post_date < 32140800 && item.post_date > 2678400) {
      dateRender = <p className="time">{`Il y a ${Math.floor(item.post_date / 2678400)} mois`}</p>
    }
    if (item.post_date < 321408000 && item.post_date > 32140800) {
      dateRender = <p className="time">{`Il y a ${Math.floor(item.post_date / 32140800)} an(s)`}</p>
    }
    if (likedPosts.includes(item.post_id)) {
      likePostButton = (
        <button type="button" className="like liked" onClick={() => this.dislikePost(item.post_id)}>
          <span className="nb_likes">{item.likes}</span>
        </button>
      )
    } else {
      likePostButton = (
        <button type="button" className="like" onClick={() => this.likePost(item.post_id)}>
          <span className="nb_likes">{item.likes}</span>
        </button>
      )
    }
    if (user && item.user_id === user.user_id) {
      deletePostButton = (
        <button type="button" title="Supprimer le post" className="delete_post" onClick={() => this.deletePost(item.post_id)}>
          <img src="src/img/picto_friend_delete.png" alt="Supprimer le post" />
        </button>
      )
    }
    return (
      <div className="bloc border bloc_publication">
        <div className="img_publication">
          <div>
            <img src={`http://localhost:1234/uploads/${item.post_image}`} alt={item.post_image} />
          </div>
        </div>
        <div className="infos">
          <div className="profil">
            <Link to={`/profil/${item.user_id}`}><img src={`http://localhost:1234/uploads/${item.user_avatar}`} alt={`${item.user_firstname} ${item.user_lastname}`} /></Link>
          </div>
          <div className="texte">
            <p className="name"><Link to={`/profil/${item.user_id}`}>{`${item.user_firstname} ${item.user_lastname}`}</Link></p>
            <p className="rang">{item.title_name}</p>
            { dateRender }
          </div>
          { likePostButton }
          { deletePostButton }
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    likedPosts: state.posts.liked_posts
  }
}

export default connect(mapStateToProps, actions)(Posts)
