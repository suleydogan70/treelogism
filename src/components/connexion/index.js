import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import * as actions from './actions'

class Connexion extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mail: '',
      pass: '',
      submit: true,
      connected: false
    }
  }

  componentWillMount() {
    const token = localStorage.getItem('token')
    if (token !== null) {
      this.state.connected = true
    }
    return token
  }

  handleSubmit(e) {
    e.preventDefault()
    const { mail, pass } = this.state
    const { connectUser } = this.props
    connectUser({ mail, pass })
  }

  handleChange(name, value) {
    this.state[name] = value
    const { mail, pass } = this.state
    if (mail.length > 5 && pass.length > 5) {
      this.setState({ submit: false })
    } else {
      this.setState({ submit: true })
    }
  }

  render() {
    const { submit, connected } = this.state
    const { auth: { isAuth, errorMessage } } = this.props
    if (isAuth || connected) {
      return <Redirect to="/" />
    }
    return (
      <div id="offline">
        <div className="wrap">
          <div className="row offline_template" id="template_connexion">
            <div className="main_content columns large-8">
              <h1>Se connecter</h1>
              <p className="errorHandler">{errorMessage}</p>
              <form className="row collapse" onSubmit={e => this.handleSubmit(e)}>
                <div className="ligne_form columns small-12">
                  <label htmlFor="mail">
                    <input className="input_picto send" name="mail" id="mail" type="email" onChange={e => this.handleChange(e.target.name, e.target.value)} required />
                    <div className="label">Adresse mail</div>
                  </label>
                </div>
                <div className="ligne_form columns small-12">
                  <label htmlFor="pass">
                    <input className="input_picto lock" name="pass" id="pass" type="password" onChange={e => this.handleChange(e.target.name, e.target.value)} required />
                    <div className="label">Mot de passe</div>
                  </label>
                  <Link to="/mdp_oublie" className="mdp_oublie">Mot de passe oublié ?</Link>
                </div>
                <button disabled={submit} type="submit" className="btn">Se connecter</button>
              </form>
            </div>
            <div className="change columns large-4">
              <h2>Bienvenue !</h2>
              <p>Psst ! Toi qui a la main verte, rejoins nous pour partager ton aventure !</p>
              <Link to="/inscription" type="button" className="btn">Inscription</Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, actions)(Connexion)
