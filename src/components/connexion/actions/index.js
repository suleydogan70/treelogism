import axios from 'axios'
import actionsType from '../../actions/types'

export const connectUser = data => (
  (dispatch) => {
    axios.post('http://localhost:1234/users/connexion', data)
      .then(
        (response) => {
          if (response.data.err) {
            return dispatch({
              type: actionsType.CONNEXION_FAIL,
              data: response.data.err
            })
          }

          localStorage.setItem('token', response.data.token)

          return dispatch({
            type: actionsType.CONNECT_USER,
            data: response.data
          })
        },
        () => (
          dispatch({
            type: actionsType.CONNEXION_FAIL,
            data: 'An error has occurend while trying to fetch the user data'
          })
        )
      )
  }
)
