export default {
  CONNECT_USER: 'CONNECT_USER',
  CONNEXION_FAIL: 'CONNEXION_FAIL',
  DELETE_ALL: 'DELETE_ALL',
  GET_POSTS: 'GET_POSTS',
  ERROR_GET_POSTS: 'ERROR_GET_POSTS',
  GET_PROFILE_POSTS: 'GET_PROFILE_POSTS',
  ERROR_GET_PROFILE_POSTS: 'ERROR_GET_PROFILE_POSTS',
  GET_LIKED_POSTS: 'GET_LIKED_POSTS',
  ERROR_GET_LIKED_POSTS: 'ERROR_GET_LIKED_POSTS',
  GET_FRIENDS: 'GET_FRIENDS',
  ERROR_GET_FRIENDS: 'ERROR_GET_FRIENDS',
  GET_PROFILE_FRIENDS: 'GET_PROFILE_FRIENDS',
  ERROR_GET_PROFILE_FRIENDS: 'ERROR_GET_PROFILE_FRIENDS',
  GET_SUGGESTIONS: 'GET_SUGGESTIONS',
  GET_PROFILE: 'GET_PROFILE',
  ERROR_GET_PROFILE: 'ERROR_GET_PROFILE',
  GET_NOTIFICATIONS: 'GET_NOTIFICATIONS',
  ERROR_GET_NOTIFICATIONS: 'ERROR_GET_NOTIFICATIONS',
  GET_PINS: 'GET_PINS',
  ERROR_GET_PINS: 'ERROR_GET_PINS',
  GET_ALL_USERS: 'GET_ALL_USERS',
  ERROR_GET_ALL_USERS: 'ERROR_GET_ALL_USERS',
  UPDATE_CONNECTED_USER: 'UPDATE_CONNECTED_USER'
}
