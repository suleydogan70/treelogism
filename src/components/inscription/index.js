import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import * as actions from './actions'

class Inscription extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstname: '',
      lastname: '',
      mail: '',
      pass: '',
      pass2: '',
      submit: true,
      connected: false
    }
  }

  componentWillMount() {
    const token = localStorage.getItem('token')
    if (token !== null) {
      this.state.connected = true
    }
    return token
  }

  handleSubmit(e) {
    e.preventDefault()
    const {
      firstname,
      lastname,
      mail,
      pass
    } = this.state
    const { userRegister } = this.props
    userRegister({
      firstname,
      lastname,
      mail,
      pass
    })
  }

  handleChange(name, value) {
    this.state[name] = value
    const {
      firstname,
      lastname,
      mail,
      pass,
      pass2
    } = this.state
    if (
      firstname.length > 0
      && lastname.length > 0
      && mail.length > 5
      && pass.length > 5
      && (pass === pass2)
    ) {
      this.setState({ submit: false })
    } else {
      this.setState({ submit: true })
    }
  }

  render() {
    const { submit, connected } = this.state
    const { auth: { isAuth, errorMessage } } = this.props
    if (isAuth || connected) {
      return <Redirect to="/" />
    }
    return (
      <div id="offline">
        <div className="wrap">
          <div className="row offline_template" id="template_inscription">
            <div className="main_content columns large-8">
              <h1>Créer un compte</h1>
              <p className="errorHandler">{errorMessage}</p>
              <form className="row collapse" onSubmit={e => this.handleSubmit(e)}>
                <div className="ligne_form left columns large-6 medium-12">
                  <label htmlFor="lastname">
                    <input name="lastname" id="lastname" type="text" onChange={e => this.handleChange(e.target.name, e.target.value)} required />
                    <div className="label">Nom</div>
                  </label>
                </div>
                <div className="ligne_form right columns large-6 medium-12">
                  <label htmlFor="firstname">
                    <input name="firstname" id="firstname" type="text" onChange={e => this.handleChange(e.target.name, e.target.value)} required />
                    <div className="label">Prénom</div>
                  </label>
                </div>
                <div className="ligne_form columns small-12">
                  <label htmlFor="mail">
                    <input name="mail" id="mail" type="email" onChange={e => this.handleChange(e.target.name, e.target.value)} required />
                    <div className="label">Adresse mail</div>
                  </label>
                </div>
                <div className="ligne_form columns small-12">
                  <label htmlFor="pass">
                    <input name="pass" id="pass" type="password" onChange={e => this.handleChange(e.target.name, e.target.value)} required />
                    <div className="label">Mot de passe</div>
                  </label>
                </div>
                <div className="ligne_form columns small-12">
                  <label htmlFor="pass2">
                    <input name="pass2" id="pass2" type="password" onChange={e => this.handleChange(e.target.name, e.target.value)} required />
                    <div className="label">Confirmer votre mot de passe</div>
                  </label>
                </div>
                <button disabled={submit} type="submit" className="btn">S&#39;inscrire !</button>
              </form>
            </div>
            <div className="change columns large-4">
              <h2>Encore vous ?!</h2>
              <p>Veuillez vous connecter avec vos identifiants pour accèder au contenu du site !</p>
              <Link to="/connexion" type="button" className="btn">Connexion</Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, actions)(Inscription)
