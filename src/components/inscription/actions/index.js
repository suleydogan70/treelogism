import axios from 'axios'
import actionsType from '../../actions/types'

export const userRegister = data => (
  (dispatch) => {
    axios.post('http://localhost:1234/users/inscription', data)
      .then(
        (response) => {
          if (response.data.err) {
            dispatch({
              type: actionsType.CONNEXION_FAIL,
              data: response.data.err
            })

            return
          }

          localStorage.setItem('token', response.data.token)

          dispatch({
            type: actionsType.CONNECT_USER,
            data: response.data
          })
        },
        () => (
          dispatch({
            type: actionsType.CONNEXION_FAIL,
            data: 'An error has occurend while trying to fetch the user data'
          })
        )
      )
  }
)
