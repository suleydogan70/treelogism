import React, { Component } from 'react'

class LostPassword extends Component {
  render() {
    return (
      <div id="offline">
        <div className="wrap">
          <div className="row offline_template" id="template_mdp_oublie">
            <div className="main_content align-center row collapse large-12">
              <h1>Récupérer son mot de passe</h1>
              <div className="columns large-8">
                <p>
                  <span>Vous pouvez réinitialiser votre mot de passe </span>
                  <span>à l&#39;aide de l&#39;adresse mail associée à votre compte</span>
                </p>
                <form className="row collapse" action="">
                  <div className="ligne_form columns small-12">
                    <label htmlFor="email">
                      <input id="email" type="email" required />
                      <div className="label">Adresse mail</div>
                    </label>
                  </div>
                  <button type="button" className="btn">Envoyer</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default LostPassword
