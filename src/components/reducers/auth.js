import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  isAuth: false,
  token: '',
  errorMessage: '',
  user: null
}

const connectUser = (state, action) => (
  fromJS(state)
    .setIn(['isAuth'], true)
    .setIn(['token'], action.data.token)
    .setIn(['user'], action.data.user)
    .setIn(['errorMessage'], '')
    .toJS()
)

const connectFail = (state, action) => (
  fromJS(state)
    .setIn(['isAuth'], false)
    .setIn(['token'], '')
    .setIn(['errorMessage'], action.data)
    .setIn(['user'], null)
    .toJS()
)

const updateConnectedUser = (state, action) => (
  fromJS(state)
    .setIn(['user'], action.data)
    .toJS()
)

const deleteAll = state => (
  fromJS(state)
    .setIn(['isAuth'], false)
    .setIn(['token'], '')
    .setIn(['errorMessage'], '')
    .setIn(['user'], null)
    .toJS()
)

const auth = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.CONNECT_USER:
      return connectUser(state, action)
    case actionsType.CONNEXION_FAIL:
      return connectFail(state, action)
    case actionsType.UPDATE_CONNECTED_USER:
      return updateConnectedUser(state, action)
    case actionsType.DELETE_ALL:
      return deleteAll(state)
    default:
      return state
  }
}

export default auth
