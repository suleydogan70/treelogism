import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  posts: [],
  liked_posts: [],
  errorMessage: ''
}

const getPosts = (state, action) => (
  fromJS(state)
    .setIn(['posts'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const getPostsFail = (state, action) => (
  fromJS(state)
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const getLikedPosts = (state, action) => (
  fromJS(state)
    .setIn(['liked_posts'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const getLikedPostsFail = (state, action) => (
  fromJS(state)
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const deleteAll = state => (
  fromJS(state)
    .setIn(['posts'], [])
    .setIn(['liked_posts'], [])
    .setIn(['errorMessage'], '')
    .toJS()
)

const posts = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.GET_POSTS:
      return getPosts(state, action)
    case actionsType.ERROR_GET_POSTS:
      return getPostsFail(state, action)
    case actionsType.GET_LIKED_POSTS:
      return getLikedPosts(state, action)
    case actionsType.ERROR_GET_LIKED_POSTS:
      return getLikedPostsFail(state, action)
    case actionsType.DELETE_ALL:
      return deleteAll(state)
    default:
      return state
  }
}

export default posts
