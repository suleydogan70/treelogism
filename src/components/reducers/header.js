import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  users: [],
  errorMessage: ''
}

const getAllUsers = (state, action) => (
  fromJS(state)
    .setIn(['users'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const errorGetUsers = (state, action) => (
  fromJS(state)
    .setIn(['user'], [])
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const deleteAll = state => (
  fromJS(state)
    .setIn(['user'], [])
    .setIn(['errorMessage'], '')
    .toJS()
)

const header = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.GET_ALL_USERS:
      return getAllUsers(state, action)
    case actionsType.ERROR_GET_ALL_USERS:
      return errorGetUsers(state, action)
    case actionsType.DELETE_ALL:
      return deleteAll(state)
    default:
      return state
  }
}

export default header
