import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  friends: [],
  posts: [],
  user: {},
  errorMessage: ''
}

const getFriends = (state, action) => (
  fromJS(state)
    .setIn(['friends'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const getPosts = (state, action) => (
  fromJS(state)
    .setIn(['posts'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const getPostsFail = (state, action) => (
  fromJS(state)
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const getFriendsFail = (state, action) => (
  fromJS(state)
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const getProfile = (state, action) => (
  fromJS(state)
    .setIn(['user'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const getProfileFail = (state, action) => (
  fromJS(state)
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const deleteAll = state => (
  fromJS(state)
    .setIn(['friends'], [])
    .setIn(['posts'], [])
    .setIn(['user'], {})
    .setIn(['errorMessage'], '')
    .toJS()
)

const profile = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.GET_PROFILE_FRIENDS:
      return getFriends(state, action)
    case actionsType.ERROR_GET_PROFILE_FRIENDS:
      return getFriendsFail(state, action)
    case actionsType.GET_PROFILE_POSTS:
      return getPosts(state, action)
    case actionsType.ERROR_GET_PROFILE_POSTS:
      return getPostsFail(state, action)
    case actionsType.GET_PROFILE:
      return getProfile(state, action)
    case actionsType.ERROR_GET_PROFILE:
      return getProfileFail(state, action)
    case actionsType.DELETE_ALL:
      return deleteAll(state)

    default:
      return state
  }
}

export default profile
