import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  pins: [],
  errorMessage: ''
}

const getPins = (state, action) => (
  fromJS(state)
    .setIn(['pins'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const errorGetPins = (state, action) => (
  fromJS(state)
    .setIn(['pins'], [])
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const deleteAll = state => (
  fromJS(state)
    .setIn(['pins'], [])
    .setIn(['errorMessage'], '')
    .toJS()
)

const pins = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.GET_PINS:
      return getPins(state, action)
    case actionsType.ERROR_GET_PINS:
      return errorGetPins(state, action)
    case actionsType.DELETE_ALL:
      return deleteAll(state)
    default:
      return state
  }
}

export default pins
