import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  friends: [],
  suggestions: [],
  errorMessage: ''
}

const getFriends = (state, action) => (
  fromJS(state)
    .setIn(['friends'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const getSuggestions = (state, action) => (
  fromJS(state)
    .setIn(['suggestions'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const getFriendsFail = (state, action) => (
  fromJS(state)
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const deleteAll = state => (
  fromJS(state)
    .setIn(['friends'], [])
    .setIn(['suggestions'], [])
    .setIn(['errorMessage'], '')
    .toJS()
)

const friends = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.GET_FRIENDS:
      return getFriends(state, action)
    case actionsType.ERROR_GET_FRIENDS:
      return getFriendsFail(state, action)
    case actionsType.GET_SUGGESTIONS:
      return getSuggestions(state, action)
    case actionsType.DELETE_ALL:
      return deleteAll(state)
    default:
      return state
  }
}

export default friends
