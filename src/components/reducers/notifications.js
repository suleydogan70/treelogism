import { fromJS } from 'immutable'
import actionsType from '../actions/types'

const DEFAULT_STATE = {
  data: [],
  errorMessage: ''
}

const getNotifications = (state, action) => (
  fromJS(state)
    .setIn(['data'], action.data)
    .setIn(['errorMessage'], '')
    .toJS()
)

const errorGetNotifications = (state, action) => (
  fromJS(state)
    .setIn(['data'], [])
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const deleteAll = state => (
  fromJS(state)
    .setIn(['data'], [])
    .setIn(['errorMessage'], '')
    .toJS()
)

const notifications = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.GET_NOTIFICATIONS:
      return getNotifications(state, action)
    case actionsType.ERROR_GET_NOTIFICATIONS:
      return errorGetNotifications(state, action)
    case actionsType.DELETE_ALL:
      return deleteAll(state)
    default:
      return state
  }
}

export default notifications
