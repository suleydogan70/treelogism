import React from 'react'

const Gallery = ({ item }) => (
  <div className="gallery_item">
    <img src={`http://localhost:1234/uploads/${item.post_image}`} alt="" />
  </div>
)

export default Gallery
