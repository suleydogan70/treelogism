import axios from 'axios'
import actionsType from '../../actions/types'

export const getProfile = userId => (
  async (dispatch) => {
    try {
      const response = await axios.get(`http://localhost:1234/users/user/${userId}`)
      if (response.data.err) {
        dispatch({
          type: actionsType.ERROR_GET_PROFILE,
          data: response.data.err
        })
      } else {
        dispatch({
          type: actionsType.GET_PROFILE,
          data: response.data.user
        })
        return response.data.user
      }
    } catch (error) {
      return dispatch({
        type: actionsType.ERROR_GET_PROFILE,
        data: 'Impossible to fetch the user'
      })
    }
    return ''
  }
)

export const getProfilePosts = userId => (
  (dispatch) => {
    axios.post('http://localhost:1234/posts/user_posts', { userId })
      .then(
        (response) => {
          if (response.data.err) {
            return dispatch({
              type: actionsType.ERROR_GET_PROFILE_POSTS,
              data: response.data.err
            })
          }
          return dispatch({
            type: actionsType.GET_PROFILE_POSTS,
            data: response.data.posts
          })
        },
        () => (
          dispatch({
            type: actionsType.ERROR_GET_PROFILE_POSTS,
            data: 'Impossible to fetch posts'
          })
        )
      )
    return true
  }
)

export const getProfileFriends = userId => (
  (dispatch) => {
    axios.post('http://localhost:1234/friends/user_friends', { userId })
      .then(
        (response) => {
          if (response.data.err) {
            return dispatch({
              type: actionsType.ERROR_GET_PROFILE_FRIENDS,
              data: response.data.err
            })
          }
          return dispatch({
            type: actionsType.GET_PROFILE_FRIENDS,
            data: response.data.friends
          })
        },
        () => (
          dispatch({
            type: actionsType.ERROR_GET_PROFILE_FRIENDS,
            data: 'Impossible to fetch friends'
          })
        )
      )
    return true
  }
)

export const updateUserInfos = (username, usermail, userdesc) => (
  async (dispatch) => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    const response = await axios.post('http://localhost:1234/users/update', { username, usermail, userdesc }, { headers })
    dispatch({
      type: actionsType.UPDATE_CONNECTED_USER,
      data: response.data.user
    })
  }
)

export const updateUserAvatar = img => (
  async (dispatch) => {
    const token = localStorage.getItem('token')
    const headers = {
      Authorization: `Bearer ${token}`
    }
    const response = await axios.post('http://localhost:1234/users/updateAvatar', img, { headers })
    dispatch({
      type: actionsType.UPDATE_CONNECTED_USER,
      data: response.data.user
    })
    dispatch({
      type: actionsType.GET_PROFILE,
      data: response.data.user
    })
    return response.data.user
  }
)
