import React, { Component } from 'react'
import { connect } from 'react-redux'
import Gallery from './components/gallery'
import Friend from '../friends/components/friend'
import * as actions from './actions'

class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      editMode: false,
      username: '',
      usermail: '',
      userdesc: '',
      userAvatar: ''
    }
  }

  componentWillMount() {
    const {
      getProfileFriends,
      getProfilePosts,
      getProfile,
      match
    } = this.props
    getProfile(match.params.id)
      .then(response => this.setState({
        username: `${response.user_firstname} ${response.user_lastname}`,
        usermail: response.user_mail,
        userdesc: response.user_description || '',
        userAvatar: response.user_avatar
      }))
    getProfileFriends(match.params.id)
    getProfilePosts(match.params.id)
  }

  componentWillReceiveProps(nextProps) {
    const { match } = this.props
    if (nextProps.match.params.id !== match.params.id) {
      const { getProfileFriends, getProfilePosts, getProfile } = this.props
      getProfile(nextProps.match.params.id)
        .then(response => this.setState({
          username: `${response.user_firstname} ${response.user_lastname}`,
          usermail: response.user_mail,
          userdesc: response.user_description || '',
          userAvatar: response.user_avatar
        }))
      getProfileFriends(nextProps.match.params.id)
      getProfilePosts(nextProps.match.params.id)
    }
  }

  updateEditMode() {
    const {
      editMode,
      username,
      usermail,
      userdesc
    } = this.state
    const { updateUserInfos } = this.props
    if (editMode) {
      updateUserInfos(username, usermail, userdesc)
    }
    this.setState({ editMode: !editMode })
  }

  checkAvatar() {
    const { updateUserAvatar } = this.props
    const formData = new FormData()
    formData.append('file', this.avatar.files[0])
    updateUserAvatar(formData)
      .then(response => this.setState({
        username: `${response.user_firstname} ${response.user_lastname}`,
        usermail: response.user_mail,
        userdesc: response.user_description || '',
        userAvatar: response.user_avatar
      }))
  }

  render() {
    const {
      friends,
      posts,
      user,
      match,
      auth,
      profilErr,
      authErr
    } = this.props
    const {
      editMode,
      username,
      usermail,
      userdesc,
      userAvatar
    } = this.state
    const width = (user.user_leafs / user.title_max_leafs) * 100
    const styles = {
      width: `${width}%`
    }
    let friendRender
    let postRender
    let userDesc
    let editProfile
    if (friends.length === 0 && auth !== null && auth.user_id === parseInt(match.params.id, 10)) {
      friendRender = <p className="error">Vous n&apos;avez pas d&apos;amis... Prenez donc un curly ! :)</p>
    } else if (friends.length === 0) {
      friendRender = <p className="error">Cet utilisateur n&apos;a pas d&apos;amis !</p>
    } else {
      friendRender = friends.map(item => (
        <Friend
          key={item.user_id}
          item={item}
          match={match.params.id}
        />
      ))
    }
    if (posts.length === 0) {
      postRender = <p className="error">Aucun post.</p>
    } else {
      postRender = (
        <div className="gallery">
          {posts.map(item => <Gallery key={item.post_id} item={item} />)}
        </div>
      )
    }
    if (!userdesc) {
      userDesc = 'Aucune description'
    } else {
      userDesc = userdesc
    }
    if (auth) {
      if ((parseInt(match.params.id, 10) === auth.user_id)) {
        if (!editMode) {
          editProfile = <span onClick={() => this.updateEditMode()} className="parametres" title="Modifier mes informations">Paramètres</span>
        } else {
          editProfile = <span onClick={() => this.updateEditMode()} className="parametres validation" title="Modifier mes informations">Paramètres</span>
        }
      }
    }
    return (
      <div id="online">
        <div className="wrap">
          {
            profilErr !== ''
              ? <p className="error">{profilErr}</p>
              : ''
          }
          {
            authErr !== ''
              ? <p className="error">{authErr}</p>
              : ''
          }
          <div className="row online_template" id="template_profil">
            <div className="grid">
              <div>
                <h2 className="title_picto picto_profil">Profil</h2>
                <div className="bloc border bloc_profil">
                  <div className="infos">
                    {editProfile}
                    <div className="profil">
                      {
                        user.user_id
                          ? (
                            <label htmlFor="avatar">
                              <input type="file" id="avatar" ref={(ref) => { this.avatar = ref }} onChange={() => this.checkAvatar()} />
                              <img src={`http://localhost:1234/uploads/${userAvatar}`} alt={`${user.user_firstname} ${user.user_lastname}`} />
                            </label>
                          )
                          : ''
                      }
                    </div>
                    <div className="texte">
                      {
                        user.user_id
                          ? !editMode
                            ? <p className="name">{username}</p>
                            : <input onChange={e => this.setState({ username: e.target.value })} type="text" value={username} />
                          : ''
                      }
                      {
                        !editMode
                          ? <p className="email">{usermail}</p>
                          : <input onChange={e => this.setState({ usermail: e.target.value })} type="text" value={usermail} />
                      }

                      <div className="experience">
                        <p className="rang">{user.title_name}</p>
                        <p className="nb_points">
                          <span className="actuel">{user.user_leafs}</span>
                          <span> / </span>
                          <span className="requis">{user.title_max_leafs}</span>
                        </p>
                        <div className="progress_bar">
                          <span className="actuel" style={styles}>actuel</span>
                        </div>
                      </div>
                    </div>
                    <div className="description">
                      {
                        !editMode
                          ? <p><span>{userDesc}</span></p>
                          : (
                            <textarea
                              onChange={e => this.setState({ userdesc: e.target.value })}
                              value={userdesc}
                            />
                          )
                      }
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <h2 className="title_picto picto_friends">
                  <span>{`${friends.length} amis`}</span>
                </h2>
                {friendRender}
              </div>
            </div>
            <div>
              <h2 className="title_picto picto_gallery">Publications</h2>
              {postRender}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    friends: state.profile.friends,
    posts: state.profile.posts,
    user: state.profile.user,
    auth: state.auth.user,
    profilErr: state.profile.errorMessage,
    authErr: state.auth.errorMessage
  }
}

export default connect(mapStateToProps, actions)(Profile)
