import axios from 'axios'
import actionsType from '../../actions/types'

export const getPins = () => (
  async (dispatch) => {
    try {
      const response = await axios.get('http://localhost:1234/maps/')
      dispatch({
        type: actionsType.GET_PINS,
        data: response.data.pins
      })
    } catch (error) {
      dispatch({
        type: actionsType.ERROR_GET_PINS,
        data: 'An error has occured while trying to fetch pings'
      })
    }
  }
)
