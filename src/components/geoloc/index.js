import React, { Component } from 'react'
import { connect } from 'react-redux'
import L from 'leaflet'
import * as actions from './actions'
import 'leaflet/dist/leaflet.css'

const styles = {
  width: '800px',
  height: '450px'
}

class MapRender extends Component {
  componentWillMount() {
    const { getPins } = this.props
    getPins()
  }

  componentDidMount() {
    this.map = L.map('map', {
      center: [48.8534, 2.3488],
      zoom: 10
    })

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      detectRetina: true,
      maxZoom: 20,
      maxNativeZoom: 17
    }).addTo(this.map)
  }

  render() {
    const { pins: { pins } } = this.props
    const LeafIcon = L.Icon.extend({
      options: {
        iconSize: [15, 20],
        popupAnchor: [0, -10]
      }
    })
    const newIcon = new LeafIcon({ iconUrl: '/src/img/pin.png' })
    if (this.map) {
      pins.map(pin => (
        L.marker([pin.latitude, pin.longitude], { icon: newIcon }).addTo(this.map)
          .bindPopup(`
            <div>
              <img src="http://localhost:1234/uploads/${pin.user_avatar}" />
            </div>
            <div>
              <p>${pin.fullname}<br><span>${pin.title_name} (<span class="requis">${pin.user_leafs}</span>)</span></p>
            </div>
          `)
      ))
    }
    return (
      <div id="online">
        <div className="wrap">
          <div className="row online_template" id="template_map">
            <h2 className="title_picto picto_map small-12">Géolocalisez-vous !</h2>
            <div className="bloc border small-12">
              <div className="iframe_container">
                <div style={styles} id="map" />
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

export default connect(state => state, actions)(MapRender)
