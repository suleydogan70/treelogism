import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Header from './components/header'
import Footer from './components/footer'
import Inscription from './components/inscription'
import Connexion from './components/connexion'
import LostPassword from './components/lostPassword'
import Home from './components/home'
import Profile from './components/profile'
import Friends from './components/friends'
import MapRender from './components/geoloc'
import NewPost from './components/newPost'

const WithHeader = () => (
  <div>
    <Header />
    <Switch>
      <Route path="/" component={Home} exact />
      <Route path="/publication" component={NewPost} exact />
      <Route path="/profil/:id" component={Profile} exact />
      <Route path="/amis" component={Friends} exact />
      <Route path="/map" component={MapRender} exact />
    </Switch>
    <Footer />
  </div>
)

class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <main>
            <Switch>
              <Route path="/inscription" component={Inscription} exact />
              <Route path="/connexion" component={Connexion} exact />
              <Route path="/mdp_oublie" component={LostPassword} exact />
              <Route component={WithHeader} />
            </Switch>
          </main>
        </div>
      </BrowserRouter>
    )
  }
}

export default Routes
