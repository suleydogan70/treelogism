import { combineReducers } from 'redux'
import auth from './components/reducers/auth'
import posts from './components/reducers/posts'
import friends from './components/reducers/friends'
import profile from './components/reducers/profile'
import notifications from './components/reducers/notifications'
import pins from './components/reducers/pins'
import header from './components/reducers/header'

export default combineReducers({
  auth,
  posts,
  friends,
  profile,
  notifications,
  pins,
  header
})
